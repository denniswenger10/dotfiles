#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Current Week
# @raycast.mode inline
# @raycast.refreshTime 60m

# Optional parameters:
# @raycast.icon 📅

# Documentation:
# @raycast.author Dennis Wenger

echo "$(date +%U)"
