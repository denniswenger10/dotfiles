#!/usr/bin/osascript

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title New Desktop
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🤖

# Documentation:
# @raycast.description Opens a new desktop space
# @raycast.author Dennis Wenger

tell application "System Events"
  do shell script "/System/Applications/Mission\\ Control.app/Contents/MacOS/Mission\\ Control"
  click button 1 of group "Spaces Bar" of group 1 of group "Mission Control" of process "Dock"
  do shell script "/System/Applications/Mission\\ Control.app/Contents/MacOS/Mission\\ Control"
end tell
