#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title New kitty
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🖥

# Documentation:
# @raycast.author Dennis Wenger

kitty -d ~
