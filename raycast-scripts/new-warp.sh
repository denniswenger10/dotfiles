#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title New Warp
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🖥

# Documentation:
# @raycast.author Dennis Wenger

open -n /Applications/Warp.app
