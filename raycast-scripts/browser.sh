#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title New browser window
# @raycast.mode silent

# Optional parameters:
# @raycast.icon 🌎
# @raycast.argument1 { "type": "text", "placeholder": "private" }

# Documentation:
# @raycast.author Dennis Wenger

qbpm launch $1
