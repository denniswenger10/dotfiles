#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Time
# @raycast.mode inline
# @raycast.refreshTime 30s

# Optional parameters:
# @raycast.icon 🕑

# Documentation:
# @raycast.author Dennis Wenger

echo "$(date +%T)"
