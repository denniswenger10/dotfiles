" Automatically install vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" General settings {

syntax on

set encoding=utf-8
set nocursorline 

" When splitting view it opens new buffer to the right and below
set splitbelow splitright

" Enable mouse for all modes
set mouse=a

" no more *.swap files
set nobackup
set nowritebackup

set number relativenumber

set clipboard=unnamedplus

" Save undo history
set undofile

" case insensitive search unless capital letters are used
set ignorecase
set smartcase

" } General settings

call plug#begin(data_dir . '/plugins')

source ~/.config/nvim/plugins/onedark.vim
source ~/.config/nvim/plugins/nvim-tree.vim
source ~/.config/nvim/plugins/vim-surround.vim
source ~/.config/nvim/plugins/vim-commentary.vim
source ~/.config/nvim/plugins/fzf.vim
source ~/.config/nvim/plugins/vim-airline.vim
source ~/.config/nvim/plugins/vim-fugitive.vim
source ~/.config/nvim/plugins/vim-gitgutter.vim
source ~/.config/nvim/plugins/auto-pairs.vim
source ~/.config/nvim/plugins/nvim-treesitter.vim
source ~/.config/nvim/plugins/rainbow.vim
source ~/.config/nvim/plugins/coc.vim
source ~/.config/nvim/plugins/lightspeed.vim
source ~/.config/nvim/plugins/winresizer.vim
" source ~/.config/nvim/plugins/firevim.vim
source ~/.config/nvim/plugins/markdown-preview.vim
source ~/.config/nvim/plugins/dart-vim-plugin.vim
source ~/.config/nvim/plugins/targets.vim
source ~/.config/nvim/plugins/vim-test.vim
source ~/.config/nvim/plugins/visual-multi.vim
source ~/.config/nvim/plugins/which-key.vim
source ~/.config/nvim/plugins/telescope.vim

" Plug 'fannheyward/coc-rust-analyzer'
" Plug 'iamcco/coc-flutter'

" Initialize plugin system
call plug#end()

lua require('plugins')

doautocmd User PlugLoaded

lua << EOF
require'nvim-tree'.setup {}
EOF

" Colors {

source $HOME/.config/nvim/themes/onedark.vim

set background=dark
set termguicolors

hi! Normal ctermbg=NONE guibg=NONE
hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE

" }

source $HOME/.config/nvim/coc.vim

" Turn off rust recommended style to use global styles instead
let g:rust_recommended_style = 0

" Spaces & Tabs {
set tabstop=2       " number of visual spaces per TAB
set softtabstop=2   " number of spaces in tab when editing
set shiftwidth=2    " number of spaces to use for autoindent
set expandtab       " tabs are space
set smartindent
set autoindent
set copyindent      " copy indent from the previous line
" } Spaces & Tabs

" Mappings {

let mapleader=" "
nmap <leader>ve :edit ~/.config/nvim/init.vim<cr>
nmap <leader>vc :edit ~/.config/nvim/coc-settings.json<cr>
nmap <leader>vr :source ~/.config/nvim/init.vim<cr>
" Maps for fzf
" Can we move these to source file?
" noremap <silent> <leader><C-f> :Files<cr>
" nnoremap <silent> <leader>f :GFiles<cr>
" nnoremap <silent> <leader>F :GFiles ~<cr>
" nmap <silent> <C-p> :Files<CR>

nnoremap <leader>ft <cmd>Telescope<cr>
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>fs <cmd>Telescope grep_string<cr>
nnoremap <leader>fe <cmd>Telescope emoji<cr>

" basic mapping to avoid the problem with fast fingers =P
:command! WQ wq
:command! Wq wq
:command! W w
:command! Q q

" Remap HJKL to move between splits
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

nnoremap <silent> <leader>sh :vertical resize +1<CR>
nnoremap <silent> <leader>sl :vertical resize -1<CR>
" nnoremap <silent> gd :call popup_create(<Plug>(coc-definition))<CR>

" } Mappings

