let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'

if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'https://github.com/joshdick/onedark.vim'

Plug 'kyazdani42/nvim-web-devicons' " for file icons
Plug 'kyazdani42/nvim-tree.lua'

Plug 'https://github.com/tpope/vim-surround'

Plug 'https://github.com/tpope/vim-commentary'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'vim-airline/vim-airline'

Plug 'tpope/vim-fugitive'

Plug 'airblade/vim-gitgutter'

Plug 'jiangmiao/auto-pairs'

Plug 'justinmk/vim-sneak'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

Plug 'luochen1990/rainbow'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-json'
Plug 'neoclide/coc-tsserver'
Plug 'neoclide/coc-rls'
Plug 'fannheyward/coc-deno'
Plug 'neoclide/coc-prettier'
" Plug 'fannheyward/coc-rust-analyzer'
" Plug 'iamcco/coc-flutter'

" Initialize plugin system
call plug#end()

lua << EOF
require'nvim-tree'.setup {}
EOF

" General settings {

set encoding=UTF-8 nocursorline 

" When splitting view it opens new buffer to the right and below
set splitbelow splitright

" Enable mouse for all modes
set mouse=a

" no more *.swap files
set nobackup
set nowritebackup

set number relativenumber

" } General settings

" Colors {

source $HOME/.config/nvim/themes/onedark.vim

set background=dark
set termguicolors

hi! Normal ctermbg=NONE guibg=NONE
hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE

" }

source $HOME/.config/nvim/coc.vim

" Turn off rust recommended style to use global styles instead
let g:rust_recommended_style = 0

" Spaces & Tabs {
set tabstop=2       " number of visual spaces per TAB
set softtabstop=2   " number of spaces in tab when editing
set shiftwidth=2    " number of spaces to use for autoindent
set expandtab       " tabs are space
set autoindent
set copyindent      " copy indent from the previous line
" } Spaces & Tabs

" Mappings {

let mapleader=" "
nnoremap <leader>s :source ~/.config/nvim/init.vim<CR>

" Maps for fzf
nnoremap <silent> <leader><C-f> :Files<cr>
nnoremap <silent> <leader>f :GFiles<cr>
nnoremap <silent> <leader>F :GFiles ~<cr>
nmap <silent> <C-p> :Files<CR>

" basic mapping to avoid the problem with fast fingers =P
:command! WQ wq
:command! Wq wq
:command! W w
:command! Q q

nnoremap <C-n> :NvimTreeToggle<CR>

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nnoremap <silent> gb <C-O><CR>

" nnoremap <silent> gd :call popup_create(<Plug>(coc-definition))<CR>

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" } Mappings

" Enable label mode for sneak
let g:sneak#label = 1

" Enable rainbow branchets
let g:rainbow_active = 1
