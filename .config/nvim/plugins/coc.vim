Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-json'
Plug 'neoclide/coc-tsserver'
Plug 'neoclide/coc-rls'
Plug 'fannheyward/coc-deno'
Plug 'neoclide/coc-prettier'

