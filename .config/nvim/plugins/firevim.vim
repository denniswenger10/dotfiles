Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

if exists('g:started_by_firenvim')
  set guifont=JetBrainsMono:h10

  let g:airline_disable_statusline = 1
  set cmdheight=1

  let g:firenvim_config = { 
    \ 'globalSettings': {
        \ 'alt': 'all',
    \  },
    \ 'localSettings': {
        \ '.*': {
            \ 'cmdline': 'neovim',
            \ 'content': 'text',
            \ 'priority': 0,
            \ 'selector': 'textarea',
            \ 'takeover': 'never',
        \ },
    \ }
\ }

  let fc = g:firenvim_config['localSettings']
  let fc['https?://gitlab\.com'] = { 'takeover': 'always', 'priority': 1 }

  augroup FirenvimOverrides
    autocmd!
    autocmd BufEnter gitlab.com_*.txt set filetype=markdown
  augroup END
endif
