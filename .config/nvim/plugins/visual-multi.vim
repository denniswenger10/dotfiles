Plug 'mg979/vim-visual-multi', {'branch': 'master'}

let g:VM_maps = {}
let g:VM_maps['Find Under']         = '<C-d>'
let g:VM_maps['Find Subword Under'] = '<C-d>'

" TODO: Figure out shortcuts for this plugin
" https://github.com/mg979/vim-visual-multi
