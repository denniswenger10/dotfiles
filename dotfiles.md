# Dotfiles information

## Get dotfiles to a new system
```bash
git init --bare $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no
config remote add origin <URL>
config pull origin master
```
